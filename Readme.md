# Development of Web Applications 2014-15

This git repository is used to support the lab assignments with several pieces of code. You may find several folders in this repository containing base code, and solutions to the proposed problems.

## Lab assignment plan

### Lab 1: Basic RESTful interface using a simple model

### Lab 2: A rich and persistent model

### Lab 3: Basic development of client code (scaffolding)

### Lab 4: Advanced client code (assynchronous)

