var url = require('url')
var model = require('./model')

var db = model.db
var resources = model.resources
var getById = model.getById

function undefinedMethod(response) {
	response.writeHead(501); // Not implemented
	response.end();
}

function error404(response) {
	response.writeHead(404); // Not found
	response.write("404 Not found");
	response.end();
}

function route(request , response) {

	var pathname = url.parse(request.url).pathname;
	var query = url.parse(request.url).query;
	var parts = pathname.split("/")

	var resourceName = parts[1];
	var id = parts[2];
	var action = parts[2];

	if( pathname == "/") 
		root(response);
	else
	if( pathname == "/favicon.ico") 
		root(response);
	else
	if( db[parts[1]] == undefined ) 
		error404(response);
	else
	if( parts[2] == undefined )         
		if( request.method == 'GET' )   // /books (GET)
			resources[parts[1]].index(response);
		else 
		if( request.method == 'POST' )  // /books (POST)
		 	resources[parts[1]].create(request, response);
		else
			undefinedMethod(response);
	else 
	if( parts[2] == "new" )  			// /books/new (GET/POST)
		resources[parts[1]].new(response);
	else
	if(parts[3] == undefined ) 
		if( request.method == 'GET' )   // /books/:id (GET)
			resources[parts[1]].get(parts[2],response);
		else 
		if( request.method == 'POST' )  // /books/:id (POST)
			resources[parts[1]].update(parts[2],request,response);
		else
			undefinedMethod(response);
	else 
	if(parts[3] == "edit" ) // /books/:id/edit (GET/POST)
		resources[parts[1]].edit(getById(parts[1],parts[2]),response); 
	else
		error404(response);
}

exports.route = route;